'use strict';

/* Filters */


var phonecatFilters = angular.module('phonecatFilters', []);

phonecatFilters.filter('checkmark', function() {
  return function(input) {
    return input ? '\u2713' : '\u2718';
  };
});

phonecatFilters.filter('notAvailable', function() {
  return function(input) {
    return ((input !== "") ? 'input' : 'Not available.');
  };
});


/*
angular.module('phonecatFilters', []).filter('checkmark', function() {
  return function(input) {
    return input ? '\u2713' : '\u2718';
  };
});


angular.module('phonecatFilterss', []).filter('notAvailable', function() {
  return function(input) {
    return (input == "") ? 'Not available.' : input;
  };
});
*/
